const http = require("http");
const fs = require("fs");
const zlib = require("zlib");
const gzip = zlib.createGzip();

http
  .createServer((req, res) => {
    //res 本身就是可写流
    const readStrem = fs.createReadStream("./index.js");
    res.writeHead(200, {
      "Content-Type": "application/x-javascript;charset=utf-8",
      "Content-Encoding": "gzip",
    });
    readStrem.pipe(gzip).pipe(res);
  })
  .listen(3000, () => {
    console.log("Server listening on port 3000");
  });

  //zlib 减少流量消耗，减轻服务器压力的优化方案
