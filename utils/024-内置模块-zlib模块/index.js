"use strict";

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;

("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;

("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;

("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;

("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
("use strict");

var acorn = require("acorn");
var walk = require("acorn/dist/walk");

function isScope(node) {
  return (
    node.type === "FunctionExpression" ||
    node.type === "FunctionDeclaration" ||
    node.type === "ArrowFunctionExpression" ||
    node.type === "Program"
  );
}
function isBlockScope(node) {
  return node.type === "BlockStatement" || isScope(node);
}

function declaresArguments(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function declaresThis(node) {
  return (
    node.type === "FunctionExpression" || node.type === "FunctionDeclaration"
  );
}

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 6,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true,
    });
  }
}
module.exports = findGlobals;
module.exports.parse = reallyParse;
function findGlobals(source) {
  var globals = [];
  var ast;
  // istanbul ignore else
  if (typeof source === "string") {
    ast = reallyParse(source);
  } else {
    ast = source;
  }
  // istanbul ignore if
  if (!(ast && typeof ast === "object" && ast.type === "Program")) {
    throw new TypeError(
      "Source must be either a string of JavaScript or an acorn AST"
    );
  }
  var declareFunction = function (node) {
    var fn = node;
    fn.locals = fn.locals || {};
    node.params.forEach(function (node) {
      declarePattern(node, fn);
    });
    if (node.id) {
      fn.locals[node.id.name] = true;
    }
  };
  var declarePattern = function (node, parent) {
    switch (node.type) {
      case "Identifier":
        parent.locals[node.name] = true;
        break;
      case "ObjectPattern":
        node.properties.forEach(function (node) {
          declarePattern(node.value, parent);
        });
        break;
      case "ArrayPattern":
        node.elements.forEach(function (node) {
          if (node) declarePattern(node, parent);
        });
        break;
      case "RestElement":
        declarePattern(node.argument, parent);
        break;
      case "AssignmentPattern":
        declarePattern(node.left, parent);
        break;
      // istanbul ignore next
      default:
        throw new Error("Unrecognized pattern type: " + node.type);
    }
  };
  var declareModuleSpecifier = function (node, parents) {
    ast.locals = ast.locals || {};
    ast.locals[node.local.name] = true;
  };
  walk.ancestor(ast, {
    VariableDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 1; i >= 0 && parent === null; i--) {
        if (
          node.kind === "var" ? isScope(parents[i]) : isBlockScope(parents[i])
        ) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      node.declarations.forEach(function (declaration) {
        declarePattern(declaration.id, parent);
      });
    },
    FunctionDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
      declareFunction(node);
    },
    Function: declareFunction,
    ClassDeclaration: function (node, parents) {
      var parent = null;
      for (var i = parents.length - 2; i >= 0 && parent === null; i--) {
        if (isScope(parents[i])) {
          parent = parents[i];
        }
      }
      parent.locals = parent.locals || {};
      parent.locals[node.id.name] = true;
    },
    TryStatement: function (node) {
      if (node.handler === null) return;
      node.handler.body.locals = node.handler.body.locals || {};
      node.handler.body.locals[node.handler.param.name] = true;
    },
    ImportDefaultSpecifier: declareModuleSpecifier,
    ImportSpecifier: declareModuleSpecifier,
    ImportNamespaceSpecifier: declareModuleSpecifier,
  });
  function identifier(node, parents) {
    var name = node.name;
    if (name === "undefined") return;
    for (var i = 0; i < parents.length; i++) {
      if (name === "arguments" && declaresArguments(parents[i])) {
        return;
      }
      if (parents[i].locals && name in parents[i].locals) {
        return;
      }
    }
    if (
      parents[parents.length - 2] &&
      parents[parents.length - 2].type === "TryStatement" &&
      parents[parents.length - 2].handler &&
      node === parents[parents.length - 2].handler.param
    ) {
      return;
    }
    node.parents = parents;
    globals.push(node);
  }
  walk.ancestor(ast, {
    VariablePattern: identifier,
    Identifier: identifier,
    ThisExpression: function (node, parents) {
      for (var i = 0; i < parents.length; i++) {
        if (declaresThis(parents[i])) {
          return;
        }
      }
      node.parents = parents;
      globals.push(node);
    },
  });
  var groupedGlobals = {};
  globals.forEach(function (node) {
    groupedGlobals[node.name] = groupedGlobals[node.name] || [];
    groupedGlobals[node.name].push(node);
  });
  return Object.keys(groupedGlobals)
    .sort()
    .map(function (name) {
      return { name: name, nodes: groupedGlobals[name] };
    });
}

// Use the fastest possible means to execute a task in a future turn
// of the event loop.

// linked list of tasks (single, with head node)
var head = { task: void 0, next: null };
var tail = head;
var flushing = false;
var requestFlush = void 0;
var isNodeJS = false;

function flush() {
  /* jshint loopfunc: true */

  while (head.next) {
    head = head.next;
    var task = head.task;
    head.task = void 0;
    var domain = head.domain;

    if (domain) {
      head.domain = void 0;
      domain.enter();
    }

    try {
      task();
    } catch (e) {
      if (isNodeJS) {
        // In node, uncaught exceptions are considered fatal errors.
        // Re-throw them synchronously to interrupt flushing!

        // Ensure continuation if the uncaught exception is suppressed
        // listening "uncaughtException" events (as domains does).
        // Continue in next event to avoid tick recursion.
        if (domain) {
          domain.exit();
        }
        setTimeout(flush, 0);
        if (domain) {
          domain.enter();
        }

        throw e;
      } else {
        // In browsers, uncaught exceptions are not fatal.
        // Re-throw them asynchronously to avoid slow-downs.
        setTimeout(function () {
          throw e;
        }, 0);
      }
    }

    if (domain) {
      domain.exit();
    }
  }

  flushing = false;
}

if (typeof process !== "undefined" && process.nextTick) {
  // Node.js before 0.9. Note that some fake-Node environments, like the
  // Mocha test runner, introduce a `process` global without a `nextTick`.
  isNodeJS = true;

  requestFlush = function () {
    process.nextTick(flush);
  };
} else if (typeof setImmediate === "function") {
  // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
  if (typeof window !== "undefined") {
    requestFlush = setImmediate.bind(window, flush);
  } else {
    requestFlush = function () {
      setImmediate(flush);
    };
  }
} else if (typeof MessageChannel !== "undefined") {
  // modern browsers
  // http://www.nonblocking.io/2011/06/windownexttick.html
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  requestFlush = function () {
    channel.port2.postMessage(0);
  };
} else {
  // old browsers
  requestFlush = function () {
    setTimeout(flush, 0);
  };
}

function asap(task) {
  tail = tail.next = {
    task: task,
    domain: isNodeJS && process.domain,
    next: null,
  };

  if (!flushing) {
    flushing = true;
    requestFlush();
  }
}

module.exports = asap;
