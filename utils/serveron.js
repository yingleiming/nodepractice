server.on("request", (req, res) => {
  // 1. 获取请求的 url 地址
  const { url } = req;
  // 2. 设置默认的响应内容为 404 Not found
  let content = "404 Not Found";
  // 3. 判断用户请求的是否为 / 或者 index.html 首页
  if (url === "/" || url === "index.html") {
    content = "首页";
  }
  // 4. 判断用户请求的是否为 /about.html 关于页面
  if (url === "/about.html") {
    content = "关于页面";
  }
  // 5. 设置 Content-Type 响应头，防止中文乱码
  res.setHeader("Content-Type", "text/html;charset=utf-8");
  // 6. 使用 res.end() 把内容响应给客户端
  res.end(content);
});
