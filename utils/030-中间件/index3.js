const express = require("express");

const app = express();

const HomeRouter = require("./router3/homeRouter");
const LoginRouter = require("./router3/loginRouter");
//应用级别中间件
app.use((req, res, next) => {
  console.log("验证 token是否有效:", Date.now());
  next();
});

//应用级别中间件:针对路由模块的
app.use("/home", HomeRouter);
app.use("/login", LoginRouter);

//错误级别中间件
app.use((req, res) => {
  res.status(404).send("页面跑丢了！！！！！！");
});

app.listen(3000, () => {
  console.log("server start");
});
