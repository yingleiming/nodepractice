const express = require("express");

const indexRouter = require("./router2/indexRouter");

const app = express();

//应用级别中间件
app.use((req, res, next) => {
  console.log("验证 token是否有效:", Date.now());
  next();
});

//应用级别中间件:针对路由模块的
app.use("/api", indexRouter);

app.listen(3000, () => {
  console.log("server start");
});
