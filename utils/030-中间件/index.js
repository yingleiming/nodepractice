//类似于中间件 1 经过计算或处理得到一个结果值后，将此值传给中间件 2，中间件 2 经过计算或处理得到一个结果值后，传给中间件 3，以此类推
const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("root");
});

app.get("/login", (req, res) => {
  res.send("login");
});

const func1 = (req, res, next) => {
  console.log("验证 token");
  var isValid = true;
  if (isValid) {
    res.carNo = "54321";
    next();
  } else {
    //返回错误信息
    res.send("error");
  }
};

//应用级别的中间件：注意摆放的位置
//应用级别的中间件，不考虑请求方式是 get、post都会执行
//传参，限制只响应 /home 的应用级别中间件
// app.use("/home", func1);
//响应全部：万能匹配
app.use(func1);

const func2 = (req, res, next) => {
  //查询数据库
  console.log(res.autoKey);
  res.send({ list: [1, 2, 3, 4, 5] });
};

app.get("/home", [func2]);

app.get("/list", (req, res, next) => {
  res.send("list!");
});

app.listen(80, () => {
  console.log("express server running at http://127.0.0.1");
});
