const express = require("express");

const router = express.Router();

//路由级别中间件
router.get("/home", (req, res, next) => {
  res.send("home!");
});

router.get("/login", (req, res, next) => {
  res.send("login!");
});

//导出
module.exports = router;
