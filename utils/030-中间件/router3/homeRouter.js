const express = require("express");

const router = express.Router();

//路由级别中间件
router.get("/", (req, res, next) => {
  res.send("home");
});

router.get("/swiper", (req, res, next) => {
  res.send("home swiper");
});

router.get("/slider", (req, res, next) => {
  res.send("home slider");
});

//导出
module.exports = router;
