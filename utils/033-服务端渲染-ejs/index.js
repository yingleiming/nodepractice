const express = require("express");
const app = express();

//配置模板引擎 先安装再配置 npm i ejs
app.set("views", "./views");
app.set("view engine", "ejs");

app.use(express.static("public"));
app.use("static", express.static("static"));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const HomeRouter = require("./router/homeRouter");
const LoginRouter = require("./router/loginRouter");
//应用级别中间件
app.use((req, res, next) => {
  console.log("验证 token是否有效:", Date.now());
  next();
});

//应用级别中间件
app.use("/home", HomeRouter);
app.use("/login", LoginRouter);

//错误级别中间件
app.use((req, res) => {
  res.status(404).send("页面跑丢了！！！！！！");
});

app.listen(3000, () => {
  console.log("server start");
});
