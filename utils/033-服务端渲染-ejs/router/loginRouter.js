const express = require("express");

const router = express.Router();

//路由级别中间件-响应前端 get 请求
router.get("/", (req, res, next) => {
  // res.send("<b>dba</b>");//send 片段 & json
  // res.json({ name: "dba" }); //json
  //渲染模板后返回给前端
  res.render("login", { title: "2222" }); //自动找 views 文件夹下的 login.ejs
});

router.post("/", (req, res, next) => {
  // res.send("<b>dba</b>");//send 片段 & json
  // res.json({ name: "dba" }); //json
  //渲染模板后返回给前端
  if (req.body.username == "abc" && req.body.password == "123") {
    console.log("登录成功");
    //重定向到 home
    res.redirect("/home");
  } else {
    console.log("登录失败");
    //重定向到 login
    res.redirect("login");
  }
  res.render("login", { title: "2222" }); //自动找 views 文件夹下的 login.ejs
});

//导出
module.exports = router;
