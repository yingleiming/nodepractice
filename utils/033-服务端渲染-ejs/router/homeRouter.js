const express = require("express");

const router = express.Router();

//路由级别中间件
router.get("/", (req, res, next) => {
  // res.send("home");
  res.render("home", { title: "home页面内容！！" });
});

router.get("/swiper", (req, res, next) => {
  res.send("home swiper");
});

router.get("/slider", (req, res, next) => {
  res.send("home slider");
});

router.get("/list", (req, res, next) => {
  res.send(["111", "222", "333"]);
});

//导出
module.exports = router;
