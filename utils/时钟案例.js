// 1.1 导入 fs 文件系统模块
const fs = require("fs");

// 1.2 导入 path 路径处理模块
const path = require("path");

// 1.3 匹配<style></style>标签的政策
// 其中 \s:表示空白字符；\S表示非空白字符；*标识匹配任意次
const regStyle = /<style>[\s\S]*<\/style>/;

//1.4 匹配<script></script>标签的政策
const regScript = /<script>[\s\S]*<\/script>/;

// <li>1.创建两个正则表达式,分别来匹配 style 和 script 标签</li>
// <li>2.使用 fs 模块,读取需要被处理的 html 文件</li>
// <li>3.自定义 resolveCSS方法，来写入index.css样式文件</li>
// <li>4.自定义 resolveJS方法，来写入index.js脚本文件</li>
// <li>5.自定义 resolveHTML方法，来写入index.html文件</li>

//2.1 使用fs模块读取 html文件
fs.readFile(
  path.join(__dirname, "../views/index.html"),
  "utf8",
  function (err, dataStr) {
    //2.2 读取文件失败
    if (err) {
      console.log("读取 html文件失败", err);
      return;
    }
    resolveCSS(dataStr);
    resolveScript(dataStr);
    resolveHTML(dataStr);
  }
);

// 3.1 处理css央视
function resolveCSS(htmlStr) {
  //3.2 使用正则表达式提取页面中的<style></style>标签
  const r1 = regStyle.exec(htmlStr);
  //3.3 将提取出来的样式字符串，做进一步的处理
  const newCSS = r1[0].replace("<style>", "").replace("</style>", "");
  //3.4 将处理后的样式字符串，写入到index.css文件中
  fs.writeFile(
    path.join(__dirname, "./../views/clock/index.css"),
    newCSS,
    (err) => {
      if (err) {
        return console.log("写入index.css文件失败", err.message);
      }
      console.log("写入css样式成功");
    }
  );
}

// 4.1 处理js脚本文件
function resolveScript(htmlStr) {
  //4.2 使用正则表达式提取页面中的<script></script>标签
  const r2 = regScript.exec(htmlStr);
  //4.3 将提取出来的脚本字符串，做进一步的处理
  const newJs = r2[0].replace("<script>", "").replace("</script>", "");
  //4.4 将处理后的样式字符串，写入到index.js文件中
  fs.writeFile(
    path.join(__dirname, "./../views/clock/index.js"),
    newJs,
    (err) => {
      if (err) {
        return console.log("写入index.js", err.message);
      }
      console.log("写入js脚本成功");
    }
  );
}
//5.1 处理html文件
function resolveHTML(htmlStr) {
  //5.2 使用字符串的 replace 方法把内嵌的 <style>和<script> 替换为外联的<link>和<script>标签
  const newHTML = htmlStr
    .replace(regStyle, "<link ref='stylesheet' href='./index.css'></link>")
    .replace(regScript, "<script src='./index.js'></script>");
  //5.3 将处理后的样式字符串，写入到index.html文件中
  fs.writeFile(
    path.join(__dirname, "./../views/clock/index.html"),
    newHTML,
    (err) => {
      if (err) {
        return console.log("写入index.html", err.message);
      }
      console.log("写入html文件成功");
    }
  );
}
