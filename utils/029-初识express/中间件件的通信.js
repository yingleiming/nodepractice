//类似于中间件 1 经过计算或处理得到一个结果值后，将此值传给中间件 2，中间件 2 经过计算或处理得到一个结果值后，传给中间件 3，以此类推
const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("root");
});

const func1 = (req, res, next) => {
  var isExpired = false;
  if (!isExpired) {
    //通过给 res 添加自定义属性传值
    res.autoKey = "abcdefghijklmnopqrstuvwxyz";
    next();
  }
  res.send("error");
};

const func2 = (req, res, next) => {
  //拿到 中间件 1传过来的数据
  console.log(res.autoKey);
  res.send({ list: [1, 2, 3, 4, 5] });
};

app.get("/home", [func1, func2]);

app.listen(80, () => {
  console.log("express server running at http://127.0.0.1");
});
