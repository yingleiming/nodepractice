// 1.导入 express
const express = require("express");

// 2.创建 web 服务器
const app = express();

//字符串
//匹配根路径的请求
app.get("/", (req, res) => {
  res.send("root");
});

//匹配 /about 路径的请求
app.get("/about", (req, res) => {
  res.send("root");
});

//匹配 /random.text 路径的请求
app.get("/random.text", (req, res) => {
  res.send("random.text");
});

//字符串模式
//使用字符串模式的路由路径示例
//匹配 acd 和 abcd  ?是可选的意思
app.get("/ab?cd", (req, res) => {
  res.send("ab?cd");
});
//匹配 /ab/****** https://aaa.com/detail/123456
app.get("/ab/:id", (req, res) => {
  res.send("aaaaa");
});
//如果需要传多个
app.get("/ab/:id1/:id2", (req, res) => {
  res.send("/ab/:id1/:id2");
});
//匹配 abcd abbcd abbbcd 等
app.get("/ab+cd", (req, res) => {
  res.send("ab+cd");
});
//匹配 abcd abxcd abRDDcd ab123cd 等 *：表示任意
app.get("/ab*cd", (req, res) => {
  res.send("ab*cd");
});

//匹配 abe和 abcde cd要么都写要么都不写
app.get("/ab(cd)?e", (req, res) => {
  res.send("/ab(cd)?e");
});

//使用字符串和字符串模式都不能满足需求时使用正则表达式
//正则表达式
//匹配任何路径中含有 a的路径
app.get(/a/, (req, res) => {
  res.send("/a/");
});

//匹配 butterfly dragonfly ,不匹配 butterflyman、dragonfly man 等
app.get(/.*Fly$/, (req, res) => {
  res.send("/.*Fly$/");
});

/**
 * 可以为请求提供多个回调函数，其行为类似中间件，唯一的区别是这些回调函数有可能调用 next('route')方法
 * 而略过其他路由回调函数，可以利用该机制为路由定义前提条件，如果在现有路径上继续执行没有意义，则可将控制
 * 权交给剩下的路径。
 *
 */

app.get("/explame/a", (req, res) => {
  res.send("Hello from A!");
});

//使用多个回调函数处理路由（记得指定 next 对象）
app.get(
  "/explame/b",
  (req, res, next) => {
    //如校验 token是否过期
    console.log("response will be sent by the next function...");
    next();
  },
  () => {
    res.send("Hello from B!");
  }
);

//使用回调函数数组处理路由（比较优雅）
var cb0 = function (req, res, next) {
  console.log("cb0");
  next();
};
var cb1 = function (req, res, next) {
  console.log("cb1");
  next();
};
var cb2 = function (req, res, next) {
  console.log("cb2");
  next();
};

app.get("/example/c", [cb0, cb1, cb2]);
//组合使用
app.get("/example/d", [cb0, cb1], (req, res, next) => {
  res.send("Hello World!");
});

//res.send();执行后，再 next() 就没有用了

// 3. 启动服务器
app.listen(80, () => {
  console.log("express server running at http://127.0.0.1");
});
