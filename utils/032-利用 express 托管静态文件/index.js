const express = require("express");
const app = express();

//配置静态资源
app.use(express.static("public"));
// app.use(express.static("static"));
app.use("static", express.static("static"));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const HomeRouter = require("./router/homeRouter");
const LoginRouter = require("./router/loginRouter");
//应用级别中间件
app.use((req, res, next) => {
  console.log("验证 token是否有效:", Date.now());
  next();
});

//应用级别中间件
app.use("/home", HomeRouter);
app.use("/login", LoginRouter);

//错误级别中间件
app.use((req, res) => {
  res.status(404).send("页面跑丢了！！！！！！");
});

app.listen(3000, () => {
  console.log("server start");
});
