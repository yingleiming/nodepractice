const express = require("express");

const router = express.Router();

//路由级别中间件-响应前端 get 请求
router.get("/", (req, res, next) => {
  //在浏览器搜索栏中输入?username=ylm&&pwd=123 输出{ username: 'ylm', pwd: '123' }
  console.log(req.query);
  res.send("login success!");
});
//路由级别中间件-响应前端 post 请求
router.post("/", (req, res, next) => {
  console.log(req.body); //必须配置中间件
  const { username, password } = req.body;
  if (username === "ylm" && password === "123") {
    res.send({ ok: 1 });
  } else {
    res.send({ ok: 0 });
  }
});

//导出
module.exports = router;
