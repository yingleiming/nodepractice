function render(res, data, type = "") {
  res.writeHead(200, {
    "Content-Type": `${type ? type : "application/json"}; charset=utf-8`,
  });
  res.write(data);
}

const apiRouter = {
  "/api/login": (req, res) => {
    const myURL = new URL(req.url, "http://127.0.0.1");
    console.log(myURL.searchParams.get("username"));
    const username = myURL.searchParams.get("username");
    const password = myURL.searchParams.get("password");
    if (username === "aaa" && password === "123") {
      render(res, `{ok:1}`);
    } else {
      render(res, `{ok:0}`);
    }
  },
  "/api/loginpost": (req, res) => {
    const myURL = new URL(req.url, "http://127.0.0.1");
    console.log(myURL.searchParams.get("username"));
    const username = myURL.searchParams.get("username");
    const password = myURL.searchParams.get("password");
    if (username === "aaa" && password === "123") {
      render(res, `{ok:1}`);
    } else {
      render(res, `{ok:0}`);
    }
  },
};

module.exports = apiRouter;
