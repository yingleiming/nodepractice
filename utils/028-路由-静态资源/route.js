//引入文件系统模块
const fs = require("fs");
const path = require("path");
const mime = require("mime");
// function myRouter(res, pathname) {
//   switch (pathname) {
//     case "/login":
//       res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
//       res.write(fs.readFileSync("./static/login.html"));
//       break;
//     case "/home":
//       res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
//       res.write(fs.readFileSync("./static/home.html"));
//       break;
//     default:
//       res.writeHead(404, { "Content-Type": "text/html; charset=utf-8" });
//       res.write(fs.readFileSync("./static/404.html"));
//       break;
//   }
// }

/**
 * node 的全局变量 __dirname 返回当前文件所在的绝对路径
 *  path 模块的 path.join() 方法，用于拼接路径
 *  mime npm 包，根据传入的文件名后缀，智能返回所需文件 content-type 类型
 *  / 当路径为/时直接返回 项目首页
 *
 *
 *
 */

//封装 render
function render(res, path, type = "") {
  res.writeHead(200, {
    "Content-Type": `${type ? type : "text/html"}; charset=utf-8`,
  });
  res.write(fs.readFileSync(path));
}

//处理静态文件
function readStaticFiles(req, res) {
  //获取路径
  const myURL = new URL(req.url, "http://127.0.0.1");
  // console.log(myURL.pathname);
  //__dirname 当前执行命令的绝对路径
  // console.log(__dirname, myURL.pathname);

  const pathname = path.join(__dirname, "static", myURL.pathname);
  console.log(pathname);
  // mime.getType("js");
  if (myURL.pathname === "/") {
    return false;
  }
  if (fs.existsSync(pathname)) {
    //处理显式返回
    render(res, pathname, mime.getType(myURL.pathname.split(".")[1]));
    return true;
  } else {
    return false;
  }
}

const myRouter = {
  "/login": (req, res) => {
    render(res, "./static/login.html");
  },
  "/home": (req, res) => {
    render(res, "./static/home.html");
  },
  "/": (req, res) => {
    render(res, "./static/home.html");
  },
  "/404": (req, res) => {
    //先判定是不是静态资源
    if (readStaticFiles(req, res)) {
      return;
    }
    render(res, "./static/404.html");
  },
  "/favicon.ico": (req, res) => {
    render(res, "./static/favicon.ico", "image/x-icon");
  },
};

module.exports = myRouter;
