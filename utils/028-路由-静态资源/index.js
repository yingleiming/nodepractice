const server = require("./service");

const route = require("./route.js");
const api = require("./api");

//注册路由
server.use(route);
server.use(api);

//启动服务器
server.start();
