const fs = require("fs");

let rs = fs.ReadStream("1.txt", "utf-8");

rs.on("data", (chunk) => {
  console.log("数据读取中...", chunk);
});

rs.on("end", () => {
  console.log("读取结束");
});

rs.on("error", () => {
  console.log("读取发生错误");
});
