const fs = require("fs");

const readStrem = fs.createReadStream("./1.txt", "utf-8");
const writeStrem = fs.createWriteStream("./2.txt", "utf-8");

readStrem.pipe(writeStrem);
