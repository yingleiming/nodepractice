const express = require("express");

const app = express();
//配置解析 post 请求的中间件--不用下载第三方，已经内置
app.use(express.urlencoded({ extended: false }));
//url form编码格式的 post的请求 content-type:application/x-www-form-urlencoded
//形如post 参数 username=yan&&pwd=123

//形如post 参数 {"username":"yan","pwd":123}
app.use(express.json());

const HomeRouter = require("./router/homeRouter");
const LoginRouter = require("./router/loginRouter");
//应用级别中间件
app.use((req, res, next) => {
  console.log("验证 token是否有效:", Date.now());
  next();
});

//应用级别中间件:针对路由模块的
app.use("/home", HomeRouter);
app.use("/login", LoginRouter);

//错误级别中间件
app.use((req, res) => {
  res.status(404).send("页面跑丢了！！！！！！");
});

app.listen(3000, () => {
  console.log("server start");
});
