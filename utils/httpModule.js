//导入 http模块
const http = require("http");
//创建 web服务器实例
const server = http.createServer();
//为 服务器绑定request事件
server.on("request", (req, res) => {
  //只要客户端请求我们的服务器，就会触发 request事件，从而调用这个处理函数
  console.log("收到客户端的请求了");
  //req 是请求对象，它包含了与客户端相关的数据和属性，例如：
  //req.url 是客户端请求的 URL 地址
  //req.method 是客户端请求的方式，例如：GET、POST
  //req.headers 是客户端请求的头信息
  console.log(req.url);
  console.log(req.method);
  console.log(req.headers);

  //设置 Content-Type 响应头，防止中文乱码
  res.setHeader("Content-Type", "text/html; charset=utf-8");

  //res 是响应对象，它包含了与服务器相关的数据和属性，例如：
  //res.write() 是向客户端发送响应数据的方法
  //res.end() 是结束响应的方法
  res.write("hello");
  res.write("world");
  res.end();
  //res.end() 必须在 res.write() 之后调用，否则会报错
});
//调用 listen(端口号，cb回调)方法，即可启动 web服务器
server.listen(80, () => {
  console.log("服务器启动成功了，可以通过 http://127.0.0.1 来进行访问");
});
