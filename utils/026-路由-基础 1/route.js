//引入文件系统模块
const fs = require("fs");

// function myRouter(res, pathname) {
//   switch (pathname) {
//     case "/login":
//       res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
//       res.write(fs.readFileSync("./static/login.html"));
//       break;
//     case "/home":
//       res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
//       res.write(fs.readFileSync("./static/home.html"));
//       break;
//     default:
//       res.writeHead(404, { "Content-Type": "text/html; charset=utf-8" });
//       res.write(fs.readFileSync("./static/404.html"));
//       break;
//   }
// }

//封装 render
function render(res, path, type = "") {
  res.writeHead(200, {
    "Content-Type": `${type ? type : "text/html"}; charset=utf-8`,
  });
  res.write(fs.readFileSync(path));
}

const myRouter = {
  "/login": (res) => {
    render(res, "./static/login.html");
  },
  "/home": (res) => {
    render(res, "./static/home.html");
  },
  "/404": (res) => {
    render(res, "./static/404.html");
  },
  "/favicon.ico": (res) => {
    render(res, "./static/favicon.ico", "image/x-icon");
  },
};

module.exports = myRouter;
