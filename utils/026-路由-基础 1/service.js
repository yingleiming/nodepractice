const http = require("http");
const Router = {};

function use(obj) {
  Object.assign(Router, obj);
}

function start() {
  http
    .createServer((req, res) => {
      const myURL = new URL(req.url, "http://127.0.0.1");
      try {
        Router[myURL.pathname](res);
      } catch (e) {
        Router["/404"](res);
      }
      res.end();
    })
    .listen(3000, () => {
      console.log("Server is running at http://localhost:3000");
    });
}

exports.start = start;
exports.use = use;
