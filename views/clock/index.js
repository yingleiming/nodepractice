
      window.onload = function () {
        //定时器，每隔 1 秒执行 1 次
        setInterval(() => {
          var dt = new Date();
          var HH = dt.getHours();
          var MM = dt.getMinutes();
          var ss = dt.getSeconds();
          //为页面上的元素赋值
          document.getElementById("h").innerHTML = padZero(HH);
          document.getElementById("m").innerHTML = padZero(MM);
          document.getElementById("s").innerHTML = padZero(ss);
        }, 1000);
      };
      function padZero(n) {
        return n < 10 ? "0" + n : n;
      }
    